#include "stdafx.h"
#include "DNAKMean.h"
#include "DNACenter.h"
#include "DistanceList.h"
#include <iostream>

int GetInfo(Distance *_pFirst)
{
	double min = INT_MAX;
	Distance *current = _pFirst;
	int minIndex = -1;
	int index = -1;
	while (current != nullptr)
	{
		index++;
		if(min > current -> distance)
		{
			min = current -> distance;
			//pMin = current;
			minIndex = index;
		}
		current = current -> pNext;
	}
	return minIndex;
}

void DNA_Kmean(DNACollection* collection) {
	const int K = 2; // two clusters

	int totalPoints = 0; //number of elements
	totalPoints = collection->numOfSequence;

	DNACenter pCenters[K] = { DNACenter(*(collection->pFirst)), DNACenter(*(collection->pLast)) };

	/*cout << "\nStart K-mean" << endl;
	cout << "Init center points is: ";

	for (int i = 0; i < K; i++)
	{
		cout << pCenters[i].point.ExportVector() << "\n";
	}*/

	bool endLoop = true;
	int countLoop = 0;
	do
	{
		countLoop++;
		//if(countLoop%100 == 0){
			cout<<"\nLoop: " << countLoop;//}

		DNACenter pC[K] = { pCenters[0], pCenters[1]};
		for (int i = 0; i < K; i++)
		{
			pC[i].pFrist = nullptr;
		}
		DNASequence* currentSequence = collection -> pFirst;
		for (int j = 0; j < totalPoints; j++) {
			string id = (*currentSequence)._id;

			//calculate distance form all points to all centers
			DistanceList distanceList = DistanceList(K);
			Distance *currentDis = distanceList.pFirst;
			int centerIndex = 0;
			while (currentDis != nullptr)
			{
				double distance = 0;
				for (int i = 0; i < V; i++)
				{
					double a = currentSequence -> _vector[i];
					double b = pC[centerIndex].point._vector[i];
					double dis = a - b;
					distance += dis * dis;
				}
				currentDis -> distance = distance;
				currentDis = currentDis->pNext;
				centerIndex += 1;
				int distanceOut = 1;
			}
			int nearestCenterIndex = GetInfo(distanceList.pFirst);
			int nearout = nearestCenterIndex;

			pC[nearestCenterIndex].Add(currentSequence, nearestCenterIndex);
			currentSequence = currentSequence -> pCollectionNext;
		}

		//update centers
		endLoop = true;
		for (int j = 0; j < K; j++)
		{
			pC[j].UpdateCenter();
			for(int i = 0; i < V; i++) {
				if(pC[j].point._vector[i] != pCenters[j].point._vector[i]) {
					pCenters[j].point._vector[i] = pC[j].point._vector[i];
					endLoop = false;
				}
			}
		}

		//update cluster
		if(!endLoop)
		{
			for (int j = 0; j < K; j++)
			{
				pC[j].UpdateCluster(&pCenters[j]);
			}
		}
	} while (!endLoop);

	/*for (int i = 0; i < K; i++)
	{
		pCenters[i].ExportCluster();
	}*/

	//cout << "Final center points is: (" << pCenters[0].point.ExportVector() << ") and (" << pCenters[1].point.ExportVector() << ")" << endl;
}
