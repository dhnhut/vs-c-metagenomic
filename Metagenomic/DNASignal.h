#pragma once
#include "stdafx.h"
#include <string>
#include <iostream>
#include <map>

using namespace std;

void GenerateKMerVector(double *_vector, string _DNAString, string _AdditionalString);

void GenerateGCContentVector(double *_vector, string _DNAString, string _AdditionalString);

void GenerateFOMVector(double *_vector, string _DNAString, string _AdditionalString);

void GenerateSOMVector(double *_vector, string _DNAString, string _AdditionalString);