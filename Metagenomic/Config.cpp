#include "stdafx.h"
#include "Config.h"

std::vector<string> GenerateNuVector(int lmer){
	int length = pow(4, lmer);
	std::vector<string> nuVector(length);
	int count = 0;
	int breakPoint = pow(4, lmer - 1);
	int nuIndex = 0;

	for (int l = 0; l < lmer; l++)
	{
		for (int i = 0; i < length; i++)
		{
			nuVector[count] += Nus[nuIndex];
			count++;
			if(count % breakPoint == 0)
			{
				
				if(nuIndex == 3)
					nuIndex = 0;
				else
					nuIndex++;
			}
			
		}
		count = 0;
		breakPoint = breakPoint/4;
	}
	return nuVector;
}