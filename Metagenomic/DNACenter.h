#pragma once
#include "DNASequence.h"

class DNACenter
{
public:
	DNACenter(void);
	DNACenter(DNASequence _point);
	~DNACenter(void);

	DNASequence point;
	DNASequence *pFrist, *pLast;

	void Add(DNASequence *p, int clusterId);
	void UpdateCenter();
	void UpdateCluster(DNACenter *pCenter);
	void ExportCluster();
};

