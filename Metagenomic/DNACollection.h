#pragma once
#include <iostream>
#include <fstream>
#include "DNASequence.h"

using namespace std;

class DNACollection
{
public:
	DNACollection(void);
	~DNACollection(void);

	DNASequence* pFirst;
	DNASequence* pLast;
	int numOfSequence;

	void AppendDNA(DNASequence* sequence);
	void ExportClusteringFile();
	void ExportVectorsFile();
	void ExportChartFile();
	string ExportSubChartString(vector<string> result, string readColors, bool isSubstring);
};

