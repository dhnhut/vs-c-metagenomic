#include "stdafx.h"
#include "DNAReadFile.h"

DNACollection ReadDNA()
{
	DNACollection collection = DNACollection();
	DNASequence* sequence = new DNASequence();
	ifstream myFile(fileName + ".fna");
	if(!myFile)
	{
		cout << "Cannot open input Fasta file: " << fileName;
		return collection;
	}
	string myBuf;
	int countSeqs = 0;
	while (!myFile.eof())
	{
		getline(myFile, myBuf); //get a string in a new line
		if(!myBuf.empty())
		{
			//cout<<myBuf;
			if(myBuf[0]== '>')//got new sequence
			{
				countSeqs++;
				if(countSeqs % 100 == 0) {
					cout << "SEQ: " << countSeqs << "\n";}

				if(sequence->Id() != ""){
					sequence->GenerateVector(signalId);
					//cout << "SEQUENCE " << sequence->Id() << sequence->ExportDNA() << "\n";
					collection.AppendDNA(sequence);
				}
				int pos = myBuf.find(" ");
				sequence = new DNASequence(myBuf.substr(1, pos));
				//store the old sequence first
				//sequence.Append(myBuf);
					
			}
			else//got next subsequence line
			{
				sequence->Append(myBuf);
			}
		}
	}

	//store the last sequence
	sequence->GenerateVector(signalId);
	collection.AppendDNA(sequence);
	
	return collection;
}

DNACollection ReadVector()
{
	DNACollection collection = DNACollection();
	DNASequence* sequence = new DNASequence();
	ifstream myFile(fileName + signalNames[signalId] + "Vectors.txt");
	if(!myFile)
	{
		cout << "Cannot open input Vector file: " << fileName;
		return collection;
	}
	string myBuf;
	int countSeqs = 0;
	while (!myFile.eof())
	{
		getline(myFile, myBuf); //get a string in a new line
		if(!myBuf.empty())
		{
			countSeqs++;
			if(countSeqs % 1000 == 0) {
				cout << "SEQ: " << countSeqs << "\n";}

			sequence = new DNASequence();
			sequence -> InputVector(myBuf);
			
			collection.AppendDNA(sequence);
			//store the old sequence first
			//sequence.Append(myBuf);
		}
	}	
	return collection;
}