#include "stdafx.h"
#include "DNACollection.h"

DNACollection::DNACollection(void)
{
	pFirst = nullptr;
	pLast = nullptr;
	numOfSequence = 0;
}

DNACollection::~DNACollection(void)
{
}

void DNACollection::AppendDNA(DNASequence* sequence) {
	if(pLast == nullptr) {
		pFirst = sequence;
		pLast = sequence;
	} else {
		pLast -> pNext = sequence;
		pLast -> pCollectionNext = sequence;
		pLast = sequence;
	}
	numOfSequence++;
}

void DNACollection::ExportClusteringFile(){
	ofstream myfile;
	myfile.open (fileName + signalNames[signalId] + "clusterResult.txt", std::ios::out);

	int count = 0;
	DNASequence *current = pFirst;
	while (current != nullptr)
	{
		myfile << std::to_string(current -> clusterId) << ",";
		current = current -> pCollectionNext;
		++count;
	}

	myfile.close();
}

void DNACollection::ExportVectorsFile(){
	ofstream myfile;
	myfile.open (fileName + signalNames[signalId] + "Vectors.txt", std::ios::out);

	int count = 0;
	DNASequence *current = pFirst;
	string result = "";
	while (current != nullptr)
	{
		result = current -> Id();
		int length = (sizeof(current -> _vector)/sizeof(*current -> _vector));
		for (int i = 0; i < length; i++)
		{
			//double a,b;
			//char buffer [256];
			//gets (buffer);
			//a = atof (buffer);
			result += " " + std::to_string(current -> _vector[i]);
		}
		current = current -> pCollectionNext;
		++count;
		myfile << result << "\n";
	}

	myfile.close();
}

void DNACollection::ExportChartFile() {
	unsigned int arrClustSize[15];
	int totalReads = 0;

	//r1
	//arrClustSize[0]=42189;
	//arrClustSize[1]=40771;
	//totalReads = 82960;

	//r3
	//arrClustSize[0]=47457;
	//arrClustSize[1]=45810;
	//totalReads = 93267;

	//r7
	//arrClustSize[0]=19473;
	//arrClustSize[1]=19291;
	//arrClustSize[2]=251709;
	//totalReads = 290473;

	////1-species02-class-1000read_exact1000
	arrClustSize[0]=500;
	arrClustSize[1]=500;
	totalReads = 1000;

	////1-species02-class-10000read_exact1000
	//arrClustSize[0]=2959;
	//arrClustSize[1]=7041;
	//totalReads = 10000;

	//1-species02-kingdom-10000read_exact1000
	//arrClustSize[0]=7073;
	//arrClustSize[1]=2927;
	//totalReads = 10000;

	////1-species-phylum-10000read_Exact_1000
	//arrClustSize[0]=6236;
	//arrClustSize[1]=3764;
	//totalReads = 10000;

	////2-species02-class-10000read_Exact1000
	//arrClustSize[0]=6183;
	//arrClustSize[1]=3817;
	//totalReads = 10000;

	////2-species02-kingdom-10000read_exact1000
	//arrClustSize[0]=6600;
	//arrClustSize[1]=3400;
	//totalReads = 10000;

	string strResult = "";
	vector<string> result(257);
	vector<string> subResult(257);
	vector<string> subHeaderResult(totalSpecies);
	string readColors = "var colors = [";
	vector<string> subReadColors(totalSpecies);
	string colors[] = {"'red'", "'green'", "'blue'", "'yellow'", "'brown'"};

	result[0] = "var vectors = [['Reads'";

	int count = 0;
	int currentSpecies = 0;
	int bottomCount = 0;
	int topCount = linesPerSpecies;
	bool jumpedToNextSpecies = false;
	DNASequence *current = pFirst;
	
	//header, fields name, ....
	while (current != nullptr && currentSpecies != totalSpecies)
	{
		if(bottomCount <= count && count < topCount) {
			jumpedToNextSpecies = false;
			result[0] += ",'" + current -> Id() + "'";
			subHeaderResult[currentSpecies] += ",'" + current -> Id() + "'";
			readColors += colors[currentSpecies] + ",";
			subReadColors[currentSpecies] += colors[currentSpecies] + ",";
		} else if (!jumpedToNextSpecies) {
			jumpedToNextSpecies = true;
			currentSpecies++;
			bottomCount += arrClustSize[currentSpecies];
			topCount += arrClustSize[currentSpecies];
		}
		current = current -> pCollectionNext;
		count++;
	}

	//vector values
	for (int i = 0; i < V; i++)
	{
		current = pFirst;
		result[i + 1] = "['" + to_string(i) + "'";
		count = 0;
		currentSpecies = 0;
		bottomCount = 0;
		topCount = linesPerSpecies;
		jumpedToNextSpecies = false;

		while (current != nullptr && currentSpecies != totalSpecies)
		{
			if(bottomCount <= count && count < topCount) {
				jumpedToNextSpecies = false;
				result[i + 1] += "," + to_string(current -> _vector[i]);
			} else if (!jumpedToNextSpecies) { //next species
				jumpedToNextSpecies = true;
				currentSpecies++;
				bottomCount += arrClustSize[currentSpecies];
				topCount += arrClustSize[currentSpecies];
			}
			current = current -> pCollectionNext;
			count++;
		}
	}

	strResult += ExportSubChartString(result, readColors, false);
	strResult += "\nvar subvectors = [";
	//export substrings
	current = pFirst;
	count = 0;
	currentSpecies = 0;
	bottomCount = 0;
	topCount = linesPerSpecies;
	jumpedToNextSpecies = false;
	for (int i = 0; i < V + 1; i++)
	{
		subResult[i] = "['" + to_string(i) + "'";
	}
	while (current != nullptr && currentSpecies != totalSpecies)
	{
		if(bottomCount <= count && count < topCount) {
			jumpedToNextSpecies = false;
			for (int i = 0; i < V; i++)
			{
				subResult[i + 1] += "," + to_string(current -> _vector[i]);
			}
		} else if (!jumpedToNextSpecies) { //next species
			subHeaderResult[currentSpecies] = "vectors : [['Reads'" + subHeaderResult[currentSpecies];
			subResult[0] = subHeaderResult[currentSpecies];
			subReadColors[currentSpecies] = "colors : [" + subReadColors[currentSpecies];
			strResult += ExportSubChartString(subResult, subReadColors[currentSpecies], true);
			//reset
			for (int i = 0; i < V + 1; i++)
			{
				subResult[i] = "['" + to_string(i) + "'";
			}

			jumpedToNextSpecies = true;
			currentSpecies++;
			bottomCount += arrClustSize[currentSpecies];
			topCount += arrClustSize[currentSpecies];
		}
		current = current -> pCollectionNext;
		count++;
		
	}
	strResult += "];";
	ofstream myfile;
	myfile.open (fileName + signalNames[signalId] + "Visual.js", std::ios::out);
	myfile << "var title = '" << fileName << " vector';";
	myfile << strResult;
	myfile.close();
}

string DNACollection::ExportSubChartString(vector<string> result, string readColors, bool isSubstring) {
	string strResult = "\n\n\n";
	string endLine = ";";
	if(isSubstring) {
		endLine = ",";
		strResult += "{";
	}
	for (int i = 0; i <= V; i++)
	{
		strResult += result[i] + "],";
	}
	strResult += "]" + endLine + "\n\n";
	strResult += readColors + "]" + endLine;
	if(isSubstring) {
		strResult += "},";
	}
	return strResult;
}