#include "stdafx.h"
#include "DNACenter.h"
#include <iostream>


DNACenter::DNACenter(void)
{
}

DNACenter::~DNACenter(void)
{
}

DNACenter::DNACenter(DNASequence _point) {
	point = _point;
	pLast = nullptr;
	pFrist = nullptr;
}

void DNACenter::Add(DNASequence *p, int clusterId) {
	p -> clusterId = clusterId;
	if(pFrist == nullptr) {
		pFrist = p;
		pLast = p;
	} else {
		pLast -> pNewNext = p;
		pLast = p;
	}
}

void DNACenter::UpdateCenter() {
	if(pFrist == nullptr)
		return;
	int count = 0;
	double sum[V];

	for (int i = 0; i < V; i++)
	{
		sum[i] = 0;
	}

	DNASequence *pTmp = pFrist;
	do {
		for(int i = 0; i < V; i++) {
			sum[i] += pTmp -> _vector[i];
		}
		pTmp = pTmp -> pNewNext;
		count++;
	} while (pTmp != nullptr);
	for(int i = 0; i < V; i++) {
		point._vector[i] = sum[i]/count;
	}
}

void DNACenter::UpdateCluster(DNACenter *pCenter) {
	pCenter -> pFrist = pFrist;
	pCenter -> pLast = pLast;

	if(pFrist == nullptr)
		return;
	DNASequence *pTmp = pFrist;
	do {
		pTmp -> pNext = pTmp -> pNewNext;
		pTmp -> pNewNext = nullptr;
		pTmp = pTmp -> pNext;
	} while (pTmp != nullptr);
}

void DNACenter::ExportCluster()
{
	DNASequence *current;
	current = pFrist;
	printf("\nCLUSTER MEMBERS: ");
	int numOfElement = 0;
	while (current != nullptr)
	{
		cout << current -> _id;
		//cout << current -> _id << ": " << current -> ExportVector();
		current = current -> pNext;
		numOfElement++;
		if(current != nullptr)
			cout << " - ";
	}
	printf("\nTOTAL MEMBER: %d \n", numOfElement);
}
