#include "stdafx.h"
#include "DNASequence.h"

DNASequence::DNASequence(void)
{
}

DNASequence::DNASequence(string _sid) 
{
	_DNAString = "";
	_id = _sid;
	clusterId = -1;
	pNext = nullptr;
	pNewNext = nullptr;
	pCollectionNext = nullptr;

	int size = sizeof(_vector)/sizeof(*_vector);
	for(int i = 0; i < size; i++)
	{
		_vector[i] = 0.0;
	}
}

DNASequence::~DNASequence(void)
{
}

//add subsequence to current DNA string
void DNASequence::Append(string subStr)
{
	_DNAString.append(subStr);
}

void DNASequence::GenerateAdditionalDNAString()
{
	_AdditionalDNAString = _DNAString;
	for (int i = 0; i < _DNAString.length(); i++)
	{
		switch (_DNAString[_DNAString.length() - i - 1])
		{
		case 'A':
			_AdditionalDNAString[i]  = 'T';
			break;
		case 'T':
			_AdditionalDNAString[i]  = 'A';
			break;
		case 'G':
			_AdditionalDNAString[i]  = 'C';
			break;
		case 'C':
			_AdditionalDNAString[i]  = 'G';
			break;
		}
		
	}
}

//export DNA string
string DNASequence::ExportDNA(void)
{
	return _DNAString;
}
//export DNA id
string DNASequence::Id()
{
	return _id;
}

void DNASequence::GenerateVector(int signal)
{
	GenerateAdditionalDNAString();

	if(signal == 0)
		GenerateKMerVector(_vector, _DNAString, _AdditionalDNAString);
	else if(signal == 1)
		GenerateGCContentVector(_vector, _DNAString, _AdditionalDNAString);
	else if(signal == 2)
		GenerateFOMVector(_vector, _DNAString, _AdditionalDNAString);
	else
		GenerateSOMVector(_vector, _DNAString, _AdditionalDNAString);
}

void DNASequence::InputVector(string _strVector)
{
	istringstream iss(_strVector);
    vector<string> tokens;
	copy(istream_iterator<string>(iss),
         istream_iterator<string>(),
         back_inserter<vector<string> >(tokens));
	_id = tokens[0];
	for (int i = 0; i < V; i++)
	{
		_vector[i] = atof(tokens[i+1].c_str());
	}
}

string DNASequence::ExportVector() 
{
	string strVector = "";
	int size = sizeof(_vector)/sizeof(*_vector);

	for(int i = 0; i < size; i++)
	{
		char numstr[21]; // enough to hold all numbers up to 64-bits
		sprintf_s(numstr, "%f", _vector[i]);
		strVector.append(numstr);
		if(i + 1 < size)
		{
			strVector.append(" - ");
		}
	}
	return strVector;
}

double* DNASequence::Vector()
{
	return _vector;
}