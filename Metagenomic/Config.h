 #pragma once
#include <vector>
#include <string>
#include <limits.h>
#include <stdio.h>
#include <math.h>

using namespace std;

const int KMer = 4;
const int V = 256; // vector length sqr(k,4)

const std::string Nus[] = {"A","T","G","C"};
vector<string> GenerateNuVector(int lmer);

const string diNuVector[16] = {"AA", "AT", "AG", "AC", "TA", "TT", "TG", "TC", "GA", "GT", "GG", "GC", "CA", "CT", "CG", "CC"};
const int signalId = 2; // 0: l-mer; 1: GC-Content; 2: FOM; 3 SOM
const string signalNames[] = {" - 4mer - ", " - GC - ", " - FOM - ", " - SOM - "};

const string fileName = "1-species-phylum-10000read_Exact_1000";

const int totalSpecies = 2;
const int linesPerSpecies = 50;
