#include "stdafx.h"
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <limits.h>
#include <stdio.h>

using namespace std;
using std::cout;
using std::endl;

std::string diNuVector[16] = {"AA", "AT", "AG", "AC", "TA", "TT", "TG", "TC", "GA", "GT", "GG", "GC", "CA", "CT", "CG", "CC"};
//std::string diNuVector[2] = {"AA", "TT"};
const int V = 16; // vector length

//class DNASequence
//{
//public:
//	std::string _DNAString;
//	std::string _id;
//	DNASequence* pNext;
//	DNASequence* pNewNext;
//	DNASequence* pCollectionNext;
//	double _divector[V];
//
//	DNASequence() {}
//
//	DNASequence(string _sid) 
//	{
//		_DNAString = "";
//		_id = _sid;
//		pNext = nullptr;
//		pNewNext = nullptr;
//		pCollectionNext = nullptr;
//
//		int size = sizeof(_divector)/sizeof(*_divector);
//		for(int i = 0; i < size; i++)
//		{
//			_divector[i] = 0.0;
//		}
//	}
//	~DNASequence(void) {}
//
//	//add subsequence to current DNA string
//	void Append(string subStr)
//	{
//		_DNAString.append(subStr);
//	}
//
//	//export DNA string
//	string ExportDNA(void)
//	{
//		return _DNAString;
//	}
//	//export DNA id
//	string Id()
//	{
//		return _id;
//	}
//
//	void GenerateVector()
//	{
//		int size = sizeof(_divector)/sizeof(*_divector);
//		float dividend = _DNAString.size() - 2 + 1;
//		for(int i = 0; i < size; i++)
//		{
//			for(int j = 0; j < _DNAString.size() - 1; j++)
//			{
//				if( _DNAString.compare(j, 2, diNuVector[i]) == 0)
//				{
//					_divector[i]++;
//				}
//			}
//			_divector[i] = _divector[i]/dividend;
//		}
//	}
//
//	string ExportVector() 
//	{
//		string strVector = "";
//		int size = sizeof(_divector)/sizeof(*_divector);
//
//		for(int i = 0; i < size; i++)
//		{
//			char numstr[21]; // enough to hold all numbers up to 64-bits
//			sprintf_s(numstr, "%f", _divector[i]);
//			strVector.append(numstr);
//			if(i + 1 < size)
//			{
//				strVector.append(" - ");
//			}
//		}
//		return strVector;
//	}
//
//	double* Vector()
//	{
//		return _divector;
//	}
//};

//class DNACollection
//{
//private:
//	
//public:
//	DNASequence* pFirst;
//	DNASequence* pLast;
//	//DNA_Distance * fDistance;
//	int numOfSequence;
//	DNACollection() {
//		pFirst = nullptr;
//		pLast = nullptr;
//		numOfSequence = 0;
//		//fDistance = nullptr;
//	}
//	void AppendDNA(DNASequence* sequence) {
//		if(pLast == nullptr) {
//			pFirst = sequence;
//			pLast = sequence;
//		} else {
//			pLast -> pNext = sequence;
//			pLast -> pCollectionNext = sequence;
//			pLast = sequence;
//		}
//		numOfSequence++;
//	}
//};

class DNA_Center {
	public:
		DNASequence point;
		DNASequence *pFrist, *pLast;
		
		DNA_Center(DNASequence _point) {
			point = _point;
			pLast = nullptr;
			pFrist = nullptr;
		}

		DNA_Center(){}

		void Add(DNASequence *p) {
			if(pFrist == nullptr) {
				pFrist = p;
				pLast = p;
			} else {
				pLast -> pNewNext = p;
				pLast = p;
			}
		}

		void UpdateCenter() {
			if(pFrist == nullptr)
				return;
			int count = 0;
			double sum[V] = {0, 0};//, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
			DNASequence *pTmp = pFrist;
			do {
				for(int i = 0; i < V; i++) {
					sum[i] += pTmp -> _divector[i];
				}
				pTmp = pTmp -> pNewNext;
				count++;
			} while (pTmp != nullptr);
			for(int i = 0; i < V; i++) {
				point._divector[i] = sum[i]/count;
			}
		}

		void UpdateCluster(DNA_Center *pCenter) {
			pCenter -> pFrist = pFrist;
			pCenter -> pLast = pLast;

			if(pFrist == nullptr)
				return;
			DNASequence *pTmp = pFrist;
			do {
				pTmp -> pNext = pTmp -> pNewNext;
				pTmp -> pNewNext = nullptr;
				pTmp = pTmp -> pNext;
			} while (pTmp != nullptr);
		}

		void ExportCluster()
		{
			DNASequence *current;
			current = pFrist;
			printf("\nCLUSTER MEMBERS: ");
			int numOfElement = 0;
			while (current != nullptr)
			{
				cout << current -> _id;
				//cout << current -> _id << ": " << current -> ExportVector();
				current = current -> pNext;
				numOfElement++;
				if(current != nullptr)
					cout << " - ";
			}
			printf("\nTOTAL MEMBER: %d \n", numOfElement);
		}
};

class Distance {
public:
	double distance;
	Distance *pNext;
	Distance() {
		distance = -1;
		pNext = nullptr;
	}
};

class DistanceList {
public:
	int numOfElement;
	double minDistance;
	int minDistanceIndex;
	Distance *pFirst;
	Distance *pLast;
	Distance *pMin;
	DistanceList(int _numOfElement) {
		numOfElement = _numOfElement;
		pFirst = nullptr;
		pLast = nullptr;
		pMin = nullptr;
		CreateList();
	}
	void CreateList() {
		if(numOfElement == 0)
			return;
		//init first element for list
		pFirst = &Distance();
		pMin = pFirst;
		pLast = pFirst;

		//init other elements
		for (int i = 0; i < numOfElement - 1; i++)
		{
			Distance dis = Distance();
			pLast->pNext = &dis;
			pLast = &dis;
		}
	}
};

int GetInfo(Distance *_pFirst)
{
	double min = INT_MAX;
	Distance *current = _pFirst;
	int minIndex = -1;
	int index = -1;
	while (current != nullptr)
	{
		index++;
		if(min > current -> distance)
		{
			min = current -> distance;
			//pMin = current;
			minIndex = index;
		}
		current = current -> pNext;
	}
	return minIndex;
}

void DNA_Kmean(DNACollection* collection) {
	const int K = 2; // two clusters

	int totalPoints = 0; //number of elements
	totalPoints = collection->numOfSequence;

	DNA_Center pCenters[K] = { DNA_Center(*(collection->pFirst)), DNA_Center(*(collection->pLast)) };

	cout << "\nStart K-mean" << endl;
	cout << "Init center points is: (" << pCenters[0].point.ExportVector() << ") and (" << pCenters[1].point.ExportVector() << ")";

	bool endLoop = true;
	int countLoop = 0;
	do
	{
		countLoop++;
		//if(countLoop%100 == 0){
			cout<<"\nLoop: " << countLoop;//}

		DNA_Center pC[K] = { pCenters[0], pCenters[1] };
		for (int i = 0; i < K; i++)
		{
			pC[i].pFrist = nullptr;
		}
		DNASequence* currentSequence = collection -> pFirst;
		for (int j = 0; j < totalPoints; j++) {
			string id = (*currentSequence)._id;
			//printf("Seg: %s \n", id);

			//calculate distance form all points to all centers
			DistanceList distanceList = DistanceList(K);
			Distance *currentDis = distanceList.pFirst;
			int centerIndex = 0;
			while (currentDis != nullptr)
			{
				double distance = 0;
				for (int i = 0; i < V; i++)
				{
					double a = currentSequence -> _divector[i];
					double b = pC[centerIndex].point._divector[i];
					double dis = a - b;
					distance += dis * dis;
				}
				currentDis -> distance = distance;
				currentDis = currentDis->pNext;
				centerIndex += 1;
				int distanceOut = 1;
				//printf("CENTER: %d DISTANCE: %d", 1, 1);
			}
			int nearestCenterIndex = GetInfo(distanceList.pFirst);
			int nearout = nearestCenterIndex;
			//printf(" NEAREST: %s", nearout);
			pC[nearestCenterIndex].Add(currentSequence);
			currentSequence = currentSequence -> pCollectionNext;
		}

		//update centers
		endLoop = true;
		for (int j = 0; j < K; j++)
		{
			pC[j].UpdateCenter();
			for(int i = 0; i < V; i++) {
				if(pC[j].point._divector[i] != pCenters[j].point._divector[i]) {
					pCenters[j].point._divector[i] = pC[j].point._divector[i];
					endLoop = false;
				}
			}
		}

		//update cluster
		if(!endLoop)
		{
			for (int j = 0; j < K; j++)
			{
				pC[j].UpdateCluster(&pCenters[j]);
			}
		}
	} while (!endLoop);

	for (int i = 0; i < K; i++)
	{
		pCenters[i].ExportCluster();
	}

	cout << "Final center points is: (" << pCenters[0].point.ExportVector() << ") and (" << pCenters[1].point.ExportVector() << ")" << endl;
}

DNACollection ReadDNA(char *strFileName)
{
	DNACollection collection = DNACollection();
	DNASequence* sequence = new DNASequence();
	ifstream myFile(strFileName);
	if(!myFile)
	{
		cout << "Cannot open input Fasta file " << strFileName;
		return collection;
	}
	string myBuf;
	int countSeqs = 0;
	while (!myFile.eof())
	{
		getline(myFile, myBuf); //get a string in a new line
		if(!myBuf.empty())
		{
			//cout<<myBuf;
			if(myBuf[0]== '>')//got new sequence
			{
				countSeqs++;
				if(countSeqs % 100 == 0) {
					cout << "SEQ: " << countSeqs << "\n";}

				if(sequence->Id() != ""){
					sequence->GenerateVector();
					//cout << "SEQUENCE " << sequence->Id() << sequence->ExportDNA() << "\n";
					collection.AppendDNA(sequence);
				}
				int pos = myBuf.find(" ");
				sequence = new DNASequence(myBuf.substr(1, pos));
				//store the old sequence first
				//sequence.Append(myBuf);
					
			}
			else//got next subsequence line
			{
				sequence->Append(myBuf);
			}

			if(myFile.eof()) //store the last sequence
			{
				//cout << "SEQUENCE" << sequence->Id() << sequence->ExportDNA() << "\n";
				sequence->GenerateVector();
				//cout << "VECTOR: " << sequence.ExportVector();
				collection.AppendDNA(sequence);
			}
		}
	}
	return collection;
}

void Cluster(char *strFileName) {
	DNACollection collection = ReadDNA(strFileName);
	DNASequence* pSequence = collection.pFirst;
	//while (pSequence != nullptr)
	//{
	//	//cout << "\nSEQUENCE" << pSequence->Id();
	//	////cout << pSequence->ExportDNA();
	//	//cout << "\nVECTOR: " << pSequence->ExportVector();
	//	//pSequence = pSequence->pNext;
	//}
	DNA_Kmean(&collection);
}