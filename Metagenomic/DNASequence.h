#pragma once
#include "stdafx.h"
#include "Config.h"
#include "DNASignal.h"
#include <stdio.h>
#include <string>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <stdlib.h>

using namespace std;

class DNASequence
{
public:
	DNASequence(void);
	DNASequence(string _sid);
	~DNASequence(void);

	std::string _DNAString;
	std::string _AdditionalDNAString;
	std::string _id;
	DNASequence* pNext;
	DNASequence* pNewNext;
	DNASequence* pCollectionNext;
	double _vector[V];
	int clusterId;

	void Append(string subStr);
	string ExportDNA(void);
	string Id();
	void GenerateVector(int signal);
	void GenerateAdditionalDNAString();
	void InputVector(string vector);
	string ExportVector();
	double* Vector();
};

