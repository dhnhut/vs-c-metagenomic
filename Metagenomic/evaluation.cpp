/*Author: Le Van Vinh
email: vinhlv@fit.hcmute.edu.vn
Date: 20/2/2014
*/
#include "stdafx.h"
#include "evaluation.h"
#include "Config.h"

std::vector<int> vcrPredict; //Binning result, assigning reads to clusters
unsigned int arrClustSize[15];
//unsigned int arrClustName[15];
unsigned short int iNumClust=2;
unsigned int iNumReads=93267;
ifstream myLogFile(fileName + signalNames[signalId] + "clusterResult.txt");
ofstream outFile(fileName + signalNames[signalId] + "clusterResult_eval_BM.txt");


void Evaluation()
{
	//------------------------------------------
	//r1
	//arrClustSize[0]=42189;
	//arrClustSize[1]=40771;
	//iNumReads = 82960;

	//r3
	arrClustSize[0]=47457;
	arrClustSize[1]=45810;
	iNumReads = 93267;

	//r7
	//arrClustSize[0]=19473;
	//arrClustSize[1]=19291;
	//arrClustSize[2]=251709;
	//iNumReads = 290473;

	////1-species02-class-1000read_exact1000
	//arrClustSize[0]=500;
	//arrClustSize[1]=500;
	//iNumReads = 1000;

	////1-species02-class-10000read_exact1000
	//arrClustSize[0]=2959;
	//arrClustSize[1]=7041;
	//iNumReads = 10000;

	//1-species02-kingdom-10000read_exact1000
	//arrClustSize[0]=7073;
	//arrClustSize[1]=2927;
	//iNumReads = 10000;

	////1-species-phylum-10000read_Exact_1000
	arrClustSize[0]=6236;
	arrClustSize[1]=3764;
	iNumReads = 10000;

	////2-species02-class-10000read_Exact1000
	//arrClustSize[0]=6183;
	//arrClustSize[1]=3817;
	//iNumReads = 10000;

	////2-species02-kingdom-10000read_exact1000
	//arrClustSize[0]=6600;
	//arrClustSize[1]=3400;
	//iNumReads = 10000;

	vector<vector<long>>vvA;
	for(int i=0;i<iNumClust;i++)
	{
		vector<long>vTemp;
		for(int j=0;j<iNumClust;j++)
			vTemp.push_back(0);
		vvA.push_back(vTemp);
	}

	vector<long>vMaxCl;//max for a cluster (a row)
	vector<long>vSumCl;
	vector<long>vMaxSp;//max for a species (a colunm)
	vector<long>vSumSp;

	for(int i=0;i<iNumClust;i++)
	{
		vMaxCl.push_back(0);
		vMaxSp.push_back(0);
		vSumCl.push_back(0);
		vSumSp.push_back(0);
	}

	//Compute matrix
	long iStart=0;
	long iEnd=0;
	for(int iSp=0;iSp<iNumClust;iSp++)
	{
		iStart=iEnd;
		iEnd=iEnd+arrClustSize[iSp];
		for(long iR=iStart;iR<iEnd;iR++)
			if(vcrPredict[iR] != -1)
				vvA[vcrPredict[iR]][iSp]++;
	}

	//Compute max and sum for each Cluster - each row
	for(int i=0;i<iNumClust;i++)
	{
		long max=vvA[i][0];
		long sum=vvA[i][0];
		for(int j=1;j<iNumClust;j++)
		{
			sum=sum + vvA[i][j];
			if(vvA[i][j] > max)
				max=vvA[i][j];
		}
		vMaxCl[i]=max;
		vSumCl[i]=sum;
	}

	//Compute max for each Species - each colunm
	for(int j=0;j<iNumClust;j++)
	{
		long max=vvA[0][j];
		for(int i=1;i<iNumClust;i++)
			if(vvA[i][j] > max)
				max=vvA[i][j];
		vMaxSp[j]=max;
	}
	//Assign sum for each Species
	for(int j=0;j<iNumClust;j++)
		vSumSp[j]=arrClustSize[j];

	long nAssignedRead=0;//The total number of assigned reads
	for(int i=0;i<iNumClust;i++)
		nAssignedRead=nAssignedRead + vSumCl[i];

	//Calculate Precision
	double fPrecision=0;
	for(int i=0;i<iNumClust;i++)
		fPrecision=fPrecision + (double)vMaxCl[i]/(double)nAssignedRead;
	//Calculate recall
	double frecall=0;
	for(int i=0;i<iNumClust;i++)
		frecall = frecall + (double)vMaxSp[i]/(double)iNumReads; 
	
	double fF1=2/(1/frecall + 1/fPrecision);

	cout<<endl<<"Precision value: "<<fPrecision;
	cout<<endl<<"Recall value: "<<frecall;
	cout<<endl<<"F-measure value: "<<fF1;
//	cout<<endl<<"Accuracy value: "<<maxAcc;
//	cout<<endl<<"("<<a1<<","<<a2<<","<<a3<<","<<a4<<","<<a5<<")";	

	outFile<<endl<<"Precision value: "<<fPrecision;
	outFile<<endl<<"Recall value: "<<frecall;
	outFile<<endl<<"F-measure value: "<<fF1;
	
}


void EvaluationResult()
{
	if(!myLogFile)
	{
		cout<<"Could not open file";
		return;
	}
	cout<<endl<<"Reading file";
	std::string myBuf;
	while (!myLogFile.eof())	
	{
		//cout<<endl<<"Reading file";
		getline(myLogFile, myBuf); //get a string in a new line
		int currPos=-1;
		int nextPos;
		nextPos=myBuf.find(",", currPos+1);
		while(nextPos != std::string::npos)
		{
			string myStr;
			myStr=myBuf.substr(currPos + 1, nextPos - currPos -1);
			short int clusName=stoi(myStr);
			vcrPredict.push_back(clusName);
			currPos=nextPos;
			nextPos=myBuf.find(",", currPos+1);			
		}
	}
	if(iNumReads != vcrPredict.size())
	{
		cout<<endl<<"Number of reads read is not right!";
		return;
	}
	cout<<endl<<"Number of reads: "<<vcrPredict.size();

	Evaluation();
}
