// K mean.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <iostream>

using std::cout;
using std::endl;

class Point 
{
	public: 
		double X;
		double Y;
		Point *next, *newNext;
		Point(){}
		Point(int _x, int _y)
		{
			X = _x;
			Y = _y;
			next = nullptr;
			newNext = nullptr;
		}
};

class Center
{
	public:
		Point point;
		Point *pFrist, *pLast;

		Center(Point _point)
		{
			point = _point;
			pLast = nullptr;
			pFrist = nullptr;
		}

		Center(){}

		void Add(Point *p)
		{
			if(pFrist == nullptr)
			{
				pFrist = p;
				pLast = p;
			}
			else
			{
				pLast -> newNext = p;
				pLast = p;
			}
		}

		void UpdateCenter()
		{
			if(pFrist == nullptr)
				return;
			int count = 0;
			double sumX = 0, sumY = 0;
			Point *pTmp = pFrist;
			do
			{
				count++;
				sumX += pTmp -> X;
				sumY += pTmp -> Y;
				pTmp = pTmp -> newNext;
			} while (pTmp != nullptr);
			point.X = sumX/count;
			point.Y = sumY/count;
		}
		void UpdateCluster(Center *pCenter)
		{
			pCenter -> pFrist = pFrist;
			pCenter -> pLast = pLast;

			if(pFrist == nullptr)
				return;
			Point *pTmp = pFrist;
			do
			{
				pTmp -> next = pTmp -> newNext;
				pTmp -> newNext = nullptr;
				pTmp = pTmp -> next;
			} while (pTmp != nullptr);
		}
};

void RunKmean()
{
	//Point points[] = { Point(1, 1), Point(2, 1), Point(5, 4), Point(4, 3) };
	Point points[] = { Point(1, 1), Point(2, 1), Point(3, 1), Point(3, 2), Point(1, 2), Point(5, 3), Point(6, 2), Point(7, 4) };
	int totalPoints = 0; //number of elements
	const int K = 2; //two clusters
	totalPoints = _countof(points);

	Center pCenters[K] = { Center(points[0]), Center(points[1]) };

	cout << "Start K-mean" << endl;
	cout << "Init center points is: (" << pCenters[0].point.X << ", " << pCenters[0].point.Y 
		<< ") and (" << pCenters[1].point.X << ", " << pCenters[1].point.Y << ")" << endl;
	bool endLoop = true;
	do
	{
		Center pC[K] = { pCenters[0].point, pCenters[1].point };

		//calculate distance form all points to all centers
		for (int j = 0; j < totalPoints; j++)
		{
			double t0 = pow((points[j].X - pC[0].point.X), 2) + pow((points[j].X - pC[0].point.X), 2);
			double t1 = pow((points[j].X - pC[1].point.X), 2) + pow((points[j].X - pC[1].point.X), 2);
			if(t0 <= t1)
			{
				pC[0].Add(&points[j]);
			}
			else
			{
				pC[1].Add(&points[j]);
			}
		}

		//update centers
		endLoop = true;
		for (int j = 0; j < K; j++)
		{
			pC[j].UpdateCenter();
			if(pC[j].point.X != pCenters[j].point.X || pC[j].point.Y != pCenters[j].point.Y)
			{
				pCenters[j].point.X = pC[j].point.X;
				pCenters[j].point.Y = pC[j].point.Y;
				endLoop = false;
			}
		}

		//update cluster
		if(!endLoop)
		{
			for (int j = 0; j < K; j++)
			{
				pC[j].UpdateCluster(pCenters);
			}
		}
	} while (!endLoop);
	cout << "Final center points is: (" << pCenters[0].point.X << ", " << pCenters[0].point.Y 
		<< ") and (" << pCenters[1].point.X << ", " << pCenters[1].point.Y << ")" << endl;
}