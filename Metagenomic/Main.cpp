#include "stdafx.h"
#include "DNACollection.h"
#include "DNAReadFile.h"
#include "DNAKMean.h"
#include "evaluation.h"

void RunKmean();

int main()
{
	//RunKmean();

	DNACollection collection = ReadDNA();
	collection.ExportVectorsFile();

	//*DNACollection collection = ReadVector();

	collection.ExportChartFile();

	DNA_Kmean(&collection);
	
	collection.ExportClusteringFile();
	//EvaluationResult();
	return 0;
}