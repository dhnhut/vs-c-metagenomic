#include "stdafx.h"
#include "DistanceList.h"


DistanceList::DistanceList(void)
{

}

DistanceList::DistanceList(int _numOfElement) {
	numOfElement = _numOfElement;
	pFirst = nullptr;
	pLast = nullptr;
	pMin = nullptr;
	CreateList();
}

DistanceList::~DistanceList(void)
{
}

void DistanceList::CreateList() {
	if(numOfElement == 0)
		return;
	//init first element for list
	pFirst = &Distance();
	pMin = pFirst;
	pLast = pFirst;

	//init other elements
	for (int i = 0; i < numOfElement - 1; i++)
	{
		Distance* dis = new Distance();
		pLast->pNext = dis;
		pLast = dis;
	}
}